# Get the count of each character in a file

if ARGV.size != 1
  puts "Usage: crount <filename>"
  exit
end

filename = ARGV[0]
file = File.new(filename)

chars = {} of String => Int32

while true

  r = file.gets(1)

  next if r == " " || r == "\n" || r == "\r"
  break if r.nil?

  if chars[r]?.nil?
    chars[r] = 1
  else
    chars[r] += 1
  end

end

keys = chars.keys
counts = chars.values

while true
	max = -1
	maxi = -1

	counts.size.times do |i|
	  if counts[i] > max
		max = counts[i]
		maxi = i
	  end
	end

	break if max < 0

	puts "#{max}\t#{keys[maxi]}"

	keys[maxi] = ""
	counts[maxi] = -1
end
